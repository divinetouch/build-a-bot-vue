module.exports = {
  configureWebpack: (config) => {
    // config.module.rules.push({
    //   target: /\.coffee$/,
    //   use: ['coffee-loader'],
    // });

    const newRule = {
      test: /\.(png|jpe?g|gif|webp)(\?.*)?$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 5000,
            fallback: {
              loader: 'file-loader',
              options: {
                name: 'img/[name].[hash:8].[ext]',
              },
            },
          },
        },
      ],
    };

    const imageRuleIndex = config.module.rules.findIndex(x => x.test.source.includes('png|jpe?g|gif'));

    config.module.rules.splice(imageRuleIndex, 1, newRule);
  },
  //   configureWebpack: {
  //     module: {
  //       // adding config for coffee script
  //       rules: [
  //         {
  //           target: /\.coffee$/,
  //           use: ['coffee-loader'],
  //         },
  //       ],
  //     },
  //   },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8081',
        changeOrigin: true,
      },
    },
  },
};
